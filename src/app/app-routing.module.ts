import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'sites',
    loadChildren: () => import('./pages/sites/sites.module').then( m => m.SitesPageModule)
  },
  {
    path: 'suites',
    loadChildren: () => import('./pages/suites/suites.module').then( m => m.SuitesPageModule)
  },
  {
    path: 'company',
    loadChildren: () => import('./pages/company/company.module').then( m => m.CompanyPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}