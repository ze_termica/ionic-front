import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Usuários com site',
      url: 'sites',
      icon: 'globe'
    },
    {
      title: 'Endereços com Suite',
      url: 'suites',
      icon: 'home'
    },
    {
      title: 'Informações de Empresas',
      url: 'company',
      icon: 'business'
    }
  ];

  constructor() {}
}
