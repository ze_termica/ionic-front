import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-sites',
  templateUrl: './sites.page.html',
  styleUrls: ['./sites.page.scss'],
})
export class SitesPage implements OnInit {
  public sites = [];

  constructor(private api: ApiService) { 
  }

  ngOnInit() {
    this.getSites();
  }

  getSites(){
    this.api.get('site').subscribe((ret: any) => {;
      ret.user_sites.forEach((element: string) => {
        this.sites.push(element);
      });
    }, err => {
      console.error(err);
    });
  }
}
