import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { User } from 'src/app/model/models';

@Component({
  selector: 'app-company',
  templateUrl: './company.page.html',
  styleUrls: ['./company.page.scss'],
})
export class CompanyPage implements OnInit {
  public users: User[] = [];
  constructor(private api: ApiService) { }

  ngOnInit() {
    this.getCompanies();
  }

  getCompanies(){
    this.api.get('company').subscribe((ret: any) => {;
      ret.users.forEach((element: User) => {
        this.users.push(element);
      });
    }, err => {
      console.error(err);
    });
  }
}