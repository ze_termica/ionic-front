import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SuitesPage } from './suites.page';

describe('SuitesPage', () => {
  let component: SuitesPage;
  let fixture: ComponentFixture<SuitesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuitesPage ],
      imports: [HttpClientTestingModule, IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuitesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
