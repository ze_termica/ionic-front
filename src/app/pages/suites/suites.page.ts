import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { User } from 'src/app/model/models';

@Component({
  selector: 'app-suites',
  templateUrl: './suites.page.html',
  styleUrls: ['./suites.page.scss'],
})
export class SuitesPage implements OnInit {
  public suites: User[] = [];

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.getSuites();
  }

  getSuites(){
    this.api.get('suite').subscribe((ret: any) => {;
      ret.suite_users.forEach((element: User) => {
        this.suites.push(element);
      });
    }, err => {
      console.error(err);
    });
  }
}
